---
title: Documentation

toc_footers:
  - <a href="https://press.codes/checkout?edd_action=add_to_cart&download_id=657809">Get the theme</a>
  - <a href="https://github.com/jarednova/timber/wiki">Timber Wiki</a>
  - <a href="http://twig.sensiolabs.org/doc/templates.html">Twig for template designers</a>
  - <a href="http://kirki.org">Kirki Customizer Documentation</a>

includes:
  - shells
  - actions
  - filters
  - templating

search: true
---

# Introduction

> ### Extending the framework
> If you want to extend the Maera Framework you can write your own shells and/or child themes, or you can use one of our premium extensions.
>
> [Material Design Shell](https://press.codes/downloads/maera-material-design-shell/)
>
> [Bootstrap 3 Shell](https://press.codes/downloads/maera-bootstrap-shell/)
>
> [Foundation Shell - Coming Soon](https://press.codes/downloads/maera-foundation-shell/)
>
> [Easy Digital Downloads Integration](https://press.codes/downloads/maera-edd/)
>
> [WooCommerce Integration - Coming Soon](https://press.codes/downloads/maera-woocommerce/)

**Maera** is a developer-friendly WordPress plugin that allows you to quickly prototype sites and extend it with your own custom plugins.

**It's a kick-ass open-minded theming framework.**

It is completely open-source, licensed under the MIT license
In its heart it uses the [twig templating engine](http://twig.sensiolabs.org/) and the WordPress Customizer.

With Maera we're introducing the concept of theme **shells**.  An easy way to completely customize the theme with your own markup, css, scripts and functionality to meet your own needs!
